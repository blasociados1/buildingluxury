function valorAMoneda(valor) {
  return Number(valor).toLocaleString("da-DK", {
    style: "currency",
    currency: "EUR",
  });
}

const _valor = valorAMoneda;
export { _valor as valorAMoneda };
