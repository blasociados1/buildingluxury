const convertToCSV = function (list, headers) {
  const array = typeof list != "object" ? JSON.parse(list) : list;
  let str = "";

  // PRIMERA FILA DE HEADERS
  const cleanHeaders = headers.filter(
    (h) =>
      h.value !== "archivo" &&
      h.value !== "actions" &&
      h.value !== "ver" &&
      h.value !== "detalle" &&
      h.value !== "incidencia" &&
      h.value !== "descargar"
  );

  str +=
    cleanHeaders.map((h) => capitalize(cleanString(h.text))).join(";") +
    ";\r\n";

  // EL RESTO DE FILAS
  for (let i = 0; i < array.length; i++) {
    let line = "";

    for (const h of cleanHeaders) {
      const value = array[i][h.value];
      if (line != "") line += ";";

      if (i == 0 && value) {
        line += `"${capitalize(cleanString(value))}"`;
      } else if (value) {
        line += `"${cleanString(value)}"`;
      } else {
        line += "--";
      }
    }

    str += line + ";\r\n";
  }

  return str;
};

export const exportDataToCSV = function (data, filename, headers) {
  const finalArray = Array.from(data);

  const a = document.createElement("a");
  const csv = convertToCSV(JSON.stringify(finalArray), headers);

  const blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
  const url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = filename || "archivo.csv";
  a.click();
  window.URL.revokeObjectURL(url);
};

function cleanString(sentence) {
  if (typeof sentence == "number") sentence = sentence.toFixed(2);
  if (sentence.includes("€")) sentence = sentence.slice(0, -2);

  return (
    sentence
      .toString()
      // quita acentos
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      // quita line breaks
      .replace(/(\r\n|\n|\r)/gm, " ")
      // quita lowdash
      .replace("_", " ")
  );
}

function capitalize(phrase) {
  return phrase
    .toLowerCase()
    .split(" ")
    .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
    .join(" ");
}
