import { store } from '../store/index'

function tienePadres(type, id) {
  const state = store.state
  const valores = [
    'facturas',
    'proyectos',
    'presupuestos',
    'empleados',
    'contactos',
    'componentes',
    'marcas'
  ]
  valores.splice(valores.indexOf(type), 1)

  const filtered = Object.keys(state)
    .filter((key) => valores.includes(key))
    .reduce((obj, key) => {
      obj[key] = state[key]
      return obj
    }, {})

  let campoConId = ''
  let idCampo = ''
  let coincidencia = false

  for (const key in filtered) {
    const c = filtered[key]

    c.forEach((e) => {
      if (JSON.stringify(e).includes(id)) {
        campoConId = key
        idCampo = e.id
        coincidencia = true
      }
    })
  }

  const campo = campoConId.substring(0, campoConId.length - 1)

  return coincidencia

  // TODO: Trabajando en eliminar elementos
}

const _tienePadres = tienePadres
export { _tienePadres as tienePadres }
