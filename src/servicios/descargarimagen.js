function download(url, name, opts) {
  const nocors = "https://cors-anywhere.herokuapp.com/";
  fetch(nocors + url)
    .then((data) => {
      return data.blob();
    })
    .then((blob) => {
      const a = document.createElement("a");
      const finalURL = URL.createObjectURL(blob);
      a.href = finalURL;
      a.download = name || "download.png";
      a.click();
      window.URL.revokeObjectURL(finalURL);
    });
}

module.exports.download = download;
