import axios from 'axios'
import { store } from '../store'

export default function interceptorsSetup() {
  axios.interceptors.request.use(
    (config) => {
      console.log('Call!!!')
      return config
    },
    (err) => {
      return Promise.reject(err)
    }
  )
}
