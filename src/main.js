import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { store } from "./store";
import vuetify from "./plugins/vuetify";
import firebase from "firebase";
import VueTour from "vue-tour";

import interceptorsSetup from "./helpers/interceptors";
import { exportDataToCSV } from "./servicios/convertircsv";
import { mapState } from "vuex";

interceptorsSetup();

require("vue-tour/dist/vue-tour.css");

Vue.config.productionTip = false;

Vue.use(firebase);
Vue.use(VueTour);
Vue.mixin({
  methods: {
    exportToCSV(data, nomeArchivo, headers) {
      exportDataToCSV(data, nomeArchivo, headers);
    },
    eresAdmin() {
      return Vue.prototype.admin;
    },
    dataTableCustomSearch(value, search, item) {
      const hasValue = (val) => (val != null ? val : "");

      const query = hasValue(search);
      const wholeItem = hasValue(item);

      if (query.length < this.minimunCharachtersToSearch) return false;

      const wordsToMatch = query.toLowerCase().split(" ");

      return wordsToMatch.every((word) =>
        JSON.stringify(wholeItem).toLowerCase().includes(word)
      );
    },
  },
});

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
  mounted() {
    const value = localStorage.getItem("darkTheme");
    this.$vuetify.theme.dark = value === "true";

    // PRODUCCION
    const cliente = {
      apiKey: "AIzaSyBpuZjV4_6e9XEtgdIZeM1Iia1eD_ZOJ8s",
      authDomain: "buildingluxury2022.firebaseapp.com",
      databaseURL: "https://buildingluxury2022-default-rtdb.firebaseio.com",
      projectId: "buildingluxury2022",
      storageBucket: "buildingluxury2022.appspot.com",
      messagingSenderId: "753503066886",
      appId: "1:753503066886:web:b088c818bcac6e3bcddc87",
    };

    // Initialize Firebase
    // BBDD seleccionada
    //
    const firebaseConfig = cliente;
    firebase.initializeApp(firebaseConfig);

    this.$store.dispatch("cargarProyectos");
    this.$store.commit("setLoader", true);

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        const data = await fetch(
          "https://bl-asociados.firebaseio.com/users/admins.json"
        );
        const admins = await data.json();
        if (admins.includes(user.email)) {
          user = { ...user, role: "Administrador" };
          Vue.prototype.admin = true;
        } else {
          user = { ...user, role: "Usuario" };
          Vue.prototype.admin = false;
        }
        this.$store.commit("setUser", user);
      } else {
        this.$store.commit("setUser", null);
        this.$router.push("/");
      }
    });
  },
}).$mount("#app");
