import Vue from "vue";
import Vuetify from "vuetify/lib";
import es from "vuetify/src/locale/es.ts";

Vue.use(Vuetify);

export default new Vuetify({
  lang: {
    locales: { es },
    current: "es",
  },
  theme: {
    options: {
      customProperties: true,
    },
    dark: false,
    themes: {
      light: {
        primary: "#5195f2",
        accent: "#9F9ECC",
        secondary: "#9C27B0",
        success: "#4ea781",
        info: "#2683c5",
        warning: "#cb9749",
        error: "#f44336",
        default: "#F2F0ED",
        icon1: "#28E2EA",
        icon2: "#2683C5",
        icon3: "#707178",
        icon4: "#f64d6e",
      },
      dark: {
        primary: "#5195f2",
        accent: "#FF4081",
        secondary: "#BEA81B",
        success: "#4CAF50",
        info: "#2196F3",
        warning: "#FB8C00",
        error: "#FF5252",
        icon1: "#28E2EA",
        icon2: "#28EA78",
        icon3: "#F3ED2A",
        icon4: "#F32A95",
      },
    },
  },
});
