import Vue from "vue";
import VueRouter from "vue-router";
import AuthGuard from "@/router/authGuard";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: "/proyectos",
    name: "Proyectos",
    component: () => import("../views/Proyectos.vue"),
  },
  {
    path: "/marcas",
    name: "Marcas",
    component: () => import("../views/Marcas.vue"),
  },
  {
    path: "/detalleProyecto/:id",
    name: "Detalle de Proyecto",
    props: true,
    component: () => import("../views/DetalleProyecto.vue"),
  },
  {
    path: "/crearProyecto",
    name: "Crear / Editar Proyecto",
    component: () => import("../views/CrearProyecto.vue"),
  },
  {
    path: "/crearCliente/:id",
    name: "Crear / Editar Cliente",
    props: true,
    component: () => import("../views/CrearCliente.vue"),
  },
  {
    path: "/crearFactura/:id",
    name: "Crear / Editar Factura",
    component: () => import("../views/CrearFactura.vue"),
    props: true,
  },
  {
    path: "/crearPresupuesto",
    name: "Crear / Editar Presupuesto",
    component: () => import("../views/CrearPresupuesto.vue"),
  },
  {
    path: "/editarPresupuesto/:id",
    name: "Crear / Editar Presupuesto",
    props: true,
    component: () => import("../views/EditarPresupuesto.vue"),
  },
  {
    path: "/componente/:id",
    name: "Componente",
    props: true,
    component: () => import("../views/DetalleComponente.vue"),
  },
  {
    path: "/crearEmpleado",
    name: "Crear / Editar Empleado",
    component: () => import("../views/CrearEmpleado.vue"),
  },
  {
    path: "/crearMarca/:id",
    name: "Crear / Editar Marca",
    props: true,
    component: () => import("../views/CrearMarca.vue"),
  },
  {
    path: "/facturas",
    name: "Facturas",
    component: () => import("../views/Facturas.vue"),
  },
  {
    path: "/editarFactura/:id",
    name: "Crear / Editar Factura",
    props: true,
    component: () => import("../views/EditarFactura.vue"),
  },
  {
    path: "/detalle",
    name: "Detalles",
    component: () => import("../views/Detalle.vue"),
  },
  {
    path: "/presupuestos",
    name: "Presupuestos",
    component: () => import("../views/Presupuestos.vue"),
  },
  {
    path: "/elegirCrearPresupuesto",
    name: "elegirCrearPresupuesto",
    component: () => import("../views/ElegirCrearPresupuesto.vue"),
  },
  {
    path: "/crearPresupuestoBorrador",
    name: "crearPresupuestoBorrador",
    component: () => import("../views/CrearPresupuestoBorrador.vue"),
  },
  {
    path: "/admin",
    name: "Zona Admin",
    component: () => import("../views/AdminZone.vue"),
  },
  {
    path: "/crearPresupuestoFinal",
    name: "crearPresupuestoFinal",
    component: () => import("../views/CrearPresupuestoFinal.vue"),
  },
  {
    path: "/catalogo",
    name: "Catálogo de Componentes",
    component: () => import("../views/Catalogo.vue"),
  },
  {
    path: "/contactos",
    name: "Contactos",
    component: () => import("../views/Contactos.vue"),
  },
  {
    path: "/empleados",
    name: "Empleados",
    component: () => import("../views/Empleados.vue"),
  },
  {
    path: "/horas_empleados",
    name: "Horas Empleados",
    component: () => import("../views/HorasEmpleados.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// router.beforeResolve(AuthGuard);

export default router;
