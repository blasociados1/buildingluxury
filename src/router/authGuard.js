import { store } from "../store";

export default (to, from, next) => {
  const user = store.getters.cogerUser;
  if (!user) {
    next("/");
  } else {
    next();
  }
  setTimeout(() => window.scrollTo(0, 0), 700);
};
