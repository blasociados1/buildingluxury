import Vue from "vue";
import Vuex from "vuex";
import firebase from "firebase";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    campos: [
      "MA",
      "MB",
      "MI",
      "TC",
      "T-SA",
      "TV",
      "VC",
      "VI",
      "OTRO",
      "OC",
      "DP",
    ],
    notas_gastos: [],
    detalleCampos: [
      { key: "MA", value: "Materias Primas" },
      { key: "MB", value: "Instalación Personal Propio" },
      { key: "MI", value: "Instalación Proveedores" },
      { key: "T-SA", value: "Material de Nuestro Almacén" },
      { key: "TC", value: "Transporte de Compras" },
      { key: "TV", value: "Transporte de Ventas" },
      { key: "VC", value: "Gastos Comerciales" },
      { key: "VI", value: "Gastos de Instalación" },
      { key: "OC", value: "Obra Civil" },
      { key: "DP", value: "DP" },
      { key: "OTRO", value: "Otros" },
    ],
    seccionActual: "Login",
    proyectos: [],
    marcas: [],
    contactos: [],
    empleados: [],
    facturas: [],
    componentes: [],
    carrito: [],
    presupuestos: [],
    tipoComponentes: [],
    horasEmpleados: [],
    user: null,
    presupuestoElegido: null,
    detalleID: null,
    dialogDetalle: false,
    dialogDetalleCrear: false,
    loader: true,
  },
  mutations: {
    setLoader(state, loader) {
      state.loader = loader;
    },
    setNotas(state, notas) {
      state.notas_gastos = notas;
    },
    setDetalleID(state, id) {
      state.detalleID = id;
    },
    vaciarCarrito(state, carrito) {
      state.carrito = carrito;
    },
    setPresupuestoElegido(state, presu) {
      state.presupuestoElegido = presu;
    },
    setUser(state, user) {
      state.user = user;
    },
    addCarrito(state, componente) {
      state.carrito.push(componente);
    },
    removeComponente(state, componente) {
      for (let i = 0; i < state.carrito.length; i++) {
        if (state.carrito[i].id == componente.id) {
          state.carrito.splice(i, 1);
          break;
        }
      }
    },
    cambiarSeccion(state, seccion) {
      state.seccionActual = seccion;
    },
    cambiarComponentes(state, componentes) {
      state.componentes = componentes;
    },
    cambiarPresupuestos(state, presupuestos) {
      state.presupuestos = presupuestos;
    },
    cambiarMarcas(state, marcas) {
      state.marcas = marcas;
    },
    cambiarProyectos(state, proyectos) {
      state.proyectos = proyectos;
    },
    addProyecto(state, proyecto) {
      state.proyectos.push(proyecto);
    },
    cambiarContactos(state, contactos) {
      state.contactos = contactos;
    },
    cambiarEmpleados(state, empleados) {
      state.empleados = empleados;
    },
    cambiarFacturas(state, facturas) {
      state.facturas = facturas;
    },
    cambiarTipoComponente(state, tipoComponentes) {
      state.tipoComponentes = tipoComponentes;
    },
    cambiarHorasEmpleados(state, horasEmpleados) {
      state.horasEmpleados = horasEmpleados;
    },
  },
  getters: {
    cogerHorasEmpleados(state) {
      return state.horasEmpleados;
    },
    cogerTipoComponentes(state) {
      return state.tipoComponentes;
    },
    cogerCampos(state) {
      return state.campos;
    },
    cogerPresupuestoElegido(state) {
      return state.presupuestoElegido;
    },
    cogerUser(state) {
      return state.user;
    },
    cogerCarrito(state) {
      return state.carrito;
    },
    cogerSeccion(state) {
      return state.seccionActual;
    },
    cogerMarcas(state) {
      return state.marcas;
    },
    cogerProyectos(state) {
      return state.proyectos;
    },
    cogerContactos(state) {
      return state.contactos;
    },
    cogerEmpleados(state) {
      return state.empleados;
    },
    cogerFacturas(state) {
      return state.facturas;
    },
    cogerComponentes(state) {
      return state.componentes;
    },
    cogerPresupuestos(state) {
      return state.presupuestos;
    },
  },
  actions: {
    seccionInicial(context) {
      // console.log(this.$router);
      context.state.seccionActual = this.$router.path.replace("/", "");
    },
    cargarProyectos(context) {
      firebase
        .database()
        .ref()
        .on("value", (data) => {
          const proyectos = [];
          const marcas = [];
          const contactos = [];
          const empleados = [];
          const facturas = [];
          const componentes = [];
          const presupuestos = [];
          const gastos = [];
          const tipoComponentes = [];
          const horasempleados = [];

          let gastosIndex = 0;
          for (const key in data.val().notas_gastos) {
            if (data.val().notas_gastos.hasOwnProperty(key)) {
              const p = data.val().notas_gastos[key];
              gastos.push({ ...p, index: gastosIndex, id: key });
              gastosIndex++;
            }
          }

          for (const key in data.val().proyectos) {
            if (data.val().proyectos.hasOwnProperty(key)) {
              const p = data.val().proyectos[key];
              if (!p.timeline) {
                p.timeline = { procesos: [] };
              }
              proyectos.push({ ...p, id: key });
            }
          }

          for (const key in data.val().marcas) {
            if (data.val().marcas.hasOwnProperty(key)) {
              const p = data.val().marcas[key];
              marcas.push({ ...p, id: key });
            }
          }

          for (const key in data.val().contactos) {
            if (data.val().contactos.hasOwnProperty(key)) {
              const p = data.val().contactos[key];
              contactos.push({ ...p, id: key });
            }
          }

          for (const key in data.val().empleados) {
            if (data.val().empleados.hasOwnProperty(key)) {
              const p = data.val().empleados[key];
              empleados.push({ ...p, id: key });
            }
          }

          for (const key in data.val().facturas) {
            if (data.val().facturas.hasOwnProperty(key)) {
              const p = data.val().facturas[key];
              if (p.detalles == 0) {
                p.detalles = [];
              }
              facturas.push({ ...p, id: key });
            }
          }

          for (const key in data.val().presupuestos) {
            if (data.val().presupuestos.hasOwnProperty(key)) {
              const p = data.val().presupuestos[key];
              presupuestos.push({ ...p, id: key });
            }
          }

          for (const key in data.val().extras.tipoComponentes) {
            if (data.val().extras.tipoComponentes.hasOwnProperty(key)) {
              const p = data.val().extras.tipoComponentes[key];
              tipoComponentes.push({ nombre: p, id: key });
            }
          }

          for (const key in data.val().horasempleado) {
            if (data.val().horasempleado.hasOwnProperty(key)) {
              const p = data.val().horasempleado[key];
              horasempleados.push({ ...p, id: key });
            }
          }

          for (const key in data.val().componentes) {
            if (data.val().componentes.hasOwnProperty(key)) {
              const p = data.val().componentes[key];
              componentes.push({ ...p, id: key });

              // firebase
              //   .database()
              //   .ref('/componentes/' + key)
              //   .update({
              //     ...p,
              //     tipo: tipoComponentes.find((t) => t.nombre == p.tipo)?.id
              //   })
            }
          }

          context.commit("setNotas", gastos);
          context.commit(
            "cambiarEmpleados",
            calculoEmpleados(proyectos, empleados)
          );

          context.commit("cambiarProyectos", proyectos);
          context.commit("cambiarMarcas", marcas);
          context.commit("cambiarContactos", contactos);
          context.commit("cambiarFacturas", facturas);
          context.commit("cambiarComponentes", componentes);
          context.commit("cambiarPresupuestos", presupuestos);
          context.commit("cambiarTipoComponente", tipoComponentes);
          context.commit("cambiarHorasEmpleados", horasempleados);
        });

      setTimeout(() => {
        context.commit("setLoader", false);
      }, 3000);
    },
  },
  modules: {},
});

function calculoEmpleados(proyectos, empleados) {
  const empleadosActualizados = [...Object.values(empleados)].map((e) => {
    return { ...e, horas: 0 };
  });

  proyectos
    .filter((p) => p.estado == "Activo")
    .forEach((proyecto) => {
      proyecto.timeline.procesos.forEach((proceso) => {
        if (proceso.horas) {
          const elEmpleado = empleadosActualizados.find(
            (e) => e.id == proceso.responsable
          );
          // console.log(elEmpleado, proceso.nombre, proyecto.nombre);
          if (elEmpleado) elEmpleado.horas += Number(proceso.horas);
        }
      });
    });

  return empleadosActualizados;
}
