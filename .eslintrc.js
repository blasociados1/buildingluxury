module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: ["plugin:vue/essential", "standard", "prettier"],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: "module",
  },
  plugins: ["vue"],
  rules: {
    "vue/valid-v-slot": [
      "error",
      {
        allowModifiers: true,
      },
    ],
    "no-unused-vars": "off",
    "vue/no-unused-vars": "off",
    "no-prototype-builtins": "off",
    eqeqeq: "off",
    "no-dupe-keys": "off",
    "vue/no-unused-components": "off",
    "vue/no-mutating-props": "off",
    "vue/return-in-computed-property": "off",
    "vue/no-side-effects-in-computed-properties": "off",
    "no-unreachable": "off",
    "promise/param-names": "off",
    "vue/no-use-v-if-with-v-for": "off",
    "no-empty": "off",
    "vue/comment-directive": "off",
  },
};
